package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebService(name = "FasterXmlEndPoint", targetNamespace = "http://endpoint.tm.dragosh.ru/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface FasterXmlEndPoint {
    @WebMethod
    void save(Session session) throws IOException, JAXBException, Exception;
    @WebMethod
    void load(Session session) throws IOException, JAXBException;
}
