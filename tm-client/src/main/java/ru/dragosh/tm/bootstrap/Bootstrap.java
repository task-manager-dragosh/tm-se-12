package ru.dragosh.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.endpoint.service.*;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    @Getter
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    @Getter
    private final ProjectEndPointService projectEndPointService = new ProjectEndPointService();
    @NotNull
    @Getter
    private final TaskEndPointService taskEndPointService = new TaskEndPointService();
    @NotNull
    @Getter
    private final UserEndPointService userEndPointService = new UserEndPointService();
    @NotNull
    @Getter
    private final DataBinEndPointService dataBinEndPointService = new DataBinEndPointService();
    @NotNull
    @Getter
    private final FasterJsonEndPointService fasterJsonEndPointService = new FasterJsonEndPointService();
    @NotNull
    @Getter
    private final FasterXmlEndPointService fasterXmlEndPointService = new FasterXmlEndPointService();
    @NotNull
    @Getter
    private final JaxbJsonEndPointService jaxbJsonEndPointService = new JaxbJsonEndPointService();
    @NotNull
    @Getter
    private final JaxbXmlEndPointService jaxbXmlEndPointService = new JaxbXmlEndPointService();
    @Setter
    private Session currentSession = new Session();
    @NotNull
    private final List<Class<? extends AbstractCommand>> classes =
            new ArrayList<>(new Reflections("ru.dragosh.tm").getSubTypesOf(AbstractCommand.class));

    public void init() {
        prepareCommands();
        start();
    }

    private void start() {
        ConsoleUtil.log(MessageType.WELCOME_MESSAGE);
        ConsoleUtil.log(MessageType.HELP_MESSAGE);
        while (true) {
            @NotNull String stringCommand = ConsoleUtil.readCommand().toLowerCase();
            @Nullable AbstractCommand command = commands.get(stringCommand);

            if (command == null) {
                ConsoleUtil.log(MessageType.WRONG_COMMAND);
                continue;
            }

            try {
                command.execute();
            } catch (Exception e) {
                System.out.println("MESSAGE -> Ошибка выполнения комманды!");
            }
        }
    }

    private void prepareCommands() {
        @NotNull final Predicate<Class<? extends AbstractCommand>> commandClassPredicate = (commandClass) -> {
            try {
                return (Class.forName(commandClass.getCanonicalName()).newInstance()) instanceof AbstractCommand;
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                System.out.println("MESSAGE -> Ошибка создания инстанса команды!");
            }
            return false;
        };
        classes.stream()
                .filter(commandClassPredicate)
                .forEach(commandClass -> {
                    try {
                        AbstractCommand abstractCommand = commandClass.newInstance();
                        commands.put(abstractCommand.getName(), abstractCommand);
                    } catch (InstantiationException | IllegalAccessException e) {
                        System.out.println("MESSAGE -> Ошибка создания инстанса команды!");
                    }
                });
    }

    @Override
    public Session getCurrentSession() {
        return this.currentSession;
    }

    @Override
    public void setCurrentSession(Session session) {
        this.currentSession = session;
    }
}
