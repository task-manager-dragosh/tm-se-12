package ru.dragosh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ProjectEndPoint;
import ru.dragosh.tm.api.TaskEndPoint;
import ru.dragosh.tm.command.AbstractCommand;
import ru.dragosh.tm.endpoint.service.ProjectEndPointService;
import ru.dragosh.tm.endpoint.service.TaskEndPointService;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.util.ConsoleUtil;
import ru.dragosh.tm.util.MessageType;

public final class TaskSortedByDateFinishCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "sort tasks by date finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "(вывод задач отсортрованных по дате окончания)";
    }

    @Override
    public void execute() {
        TaskEndPointService taskEndPointService = serviceLocator.getTaskEndPointService();
        TaskEndPoint taskEndPoint = taskEndPointService.getTaskEndPointPort();
        Session session = serviceLocator.getCurrentSession();

        taskEndPoint.getSortedByDateFinish(session);
    }
}
