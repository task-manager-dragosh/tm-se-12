package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.SessionRepository;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;
import ru.dragosh.tm.exception.EntityListIsEmptyException;
import ru.dragosh.tm.exception.EntityNotExistsException;
import ru.dragosh.tm.util.FieldConst;

import java.io.File;
import java.sql.*;
import java.util.*;

public final class SessionRepositoryImplement implements SessionRepository {
    @NotNull
    @Override
    public List<Session> findAll(@NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from session";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        return fetchMultiple(resultSet);
    }

    @NotNull
    @Override
    public Session findOne(@NotNull final String sessionId,
                           @NotNull final Connection connection) throws Exception {
        @NotNull final String query = "select * from session where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Session session = fetchOne(resultSet);
        if (session == null) throw new EntityNotExistsException();
        return session;
    }

    @Override
    public void persist(@NotNull final Session session,
                        @NotNull final Connection connection) throws EntityIsAlreadyExistException, SQLException {
        @NotNull final String entityId = session.getId();
        @NotNull final String query = "select * from session where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, entityId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        Session session2 = fetchOne(resultSet);
        if (session2 != null)
            throw new EntityIsAlreadyExistException();

        @NotNull final String insertQuery = "insert into session (id, time_stamp, user_id, signature) values (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatementInsert = connection.prepareStatement(insertQuery);
        preparedStatementInsert.setString(1, session.getId());
        preparedStatementInsert.setLong(2, session.getTimeStamp());
        preparedStatementInsert.setString(3, session.getUserId());
        preparedStatementInsert.setString(4, session.getSignature());
    }

    @Override
    public void merge(@NotNull final String sessionId,
                      @NotNull final Session session,
                      @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "update session set time_stamp = ?, user_id = ?, signature = ? where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setLong(1, session.getTimeStamp());
        preparedStatement.setString(2, session.getUserId());
        preparedStatement.setString(3, session.getSignature());
        preparedStatement.setString(4, sessionId);
        preparedStatement.executeUpdate();
    }

    @Override
    public void remove(@NotNull final String sessionId,
                       @NotNull final Connection connection) throws Exception {
        @NotNull final String query = "delete from session where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        preparedStatement.executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final Connection connection) throws Exception {
        @NotNull final String query = "delete from session";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
    }

    @Override
    public boolean contains(@NotNull final String id,
                            @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from session where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.next();
    }

    private Session fetchOne(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        resultSet.next();
        return _fetch(resultSet);
    }

    private List<Session> fetchMultiple(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final List<Session> sessions = new ArrayList<>();
        while (resultSet.next()) {
            sessions.add(_fetch(resultSet));
        }
        return sessions;
    }

    private Session _fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        Session session = new Session();
        session.setId(resultSet.getString(FieldConst.ID));
        session.setSignature(resultSet.getString(FieldConst.SIGNATURE));
        session.setTimeStamp(resultSet.getLong(FieldConst.TIME_STAMP));
        session.setUserId(resultSet.getString(FieldConst.USER_ID));
        return session;
    }
}
