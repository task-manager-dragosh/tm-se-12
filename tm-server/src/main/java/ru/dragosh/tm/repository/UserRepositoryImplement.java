package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.util.EntityUtil;
import ru.dragosh.tm.util.FieldConst;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class UserRepositoryImplement implements UserRepository {
    @Nullable
    @Override
    public User find(@NotNull final String login,
                     @NotNull final String password,
                     @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from app_user where login = ? and hash_password = ? limit 1";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, password);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchOne(resultSet);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login,
                            @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from app_user where login = ? limit 1";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchOne(resultSet);
    }

    @Override
    public User findById(@NotNull final String id,
                         @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from app_user where id = ? limit 1";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchOne(resultSet);
    }

    @Override
    public void persist(@NotNull final User user,
                        @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "insert into app_user (id, login, hash_password, role_id) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
    }

    @Override
    public void merge(@NotNull final User user,
                      @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "update app_user set login = ?, hash_password = ?, role_id = ? where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getRole().getId());
        preparedStatement.setString(4, user.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public void remove(@NotNull final String userId,
                       @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "delete from app_user where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
    }

    @Override
    public List<User> getEntitiesList(@NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchMultiple(resultSet);
    }

    @Override
    public void loadEntities(@NotNull final List<User> entities,
                             @NotNull final Connection connection) throws SQLException {
        @NotNull final String deleteQuery = "delete from app_user";
        @NotNull PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.executeUpdate();

        @NotNull final String insertQuery = "insert into app_user(id, login, hash_password, role_id) values (?, ?, ?, ?)";
        for (User user: entities) {
            preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, user.getId());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getRole().getId());
            preparedStatement.executeUpdate();
        }
    }

    private User fetchOne(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        resultSet.next();
        return fetchOne(resultSet);
    }

    private List<User> fetchMultiple(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            users.add(_fetch(resultSet));
        }
        return users;
    }

    private User _fetch(@NotNull final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final User user = new User();
        user.setId(resultSet.getString(FieldConst.ID));
        user.setLogin(resultSet.getString((FieldConst.LOGIN)));
        user.setPassword(resultSet.getString(FieldConst.HASH_PASSWORD));
        user.setRole(EntityUtil.getRoleById(resultSet.getString(FieldConst.ROLE_ID)));
        return user;
    }
}
