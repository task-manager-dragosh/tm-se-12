package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.TaskRepository;
import ru.dragosh.tm.entity.Entity;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.util.FieldConst;

import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepositoryImplement implements TaskRepository {
    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId,
                              @NotNull final String projectId,
                              @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from task where user_id = ? and project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchMultiple(resultSet);
    }

    @Nullable
    @Override
    public Task find(@NotNull final String userId,
                     @NotNull final String projectId,
                     @NotNull final String taskName,
                     @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from task where user_id = ? and project_id = ? and task_name = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.setString(3, taskName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchOne(resultSet);
    }

    @Override
    public void removeAll(@NotNull final String userId,
                          @NotNull final String projectId,
                          @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "delete from task where user_id = ? and project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findByStringPart(@NotNull final String userId,
                                       @NotNull final String projectId,
                                       @NotNull final String str,
                                       @NotNull final Connection connection) throws SQLException {
        @NotNull final String regexp = "'*" + str + "*'";
        @NotNull final String query = "select * from task where user_id = ? and project_id = ? and regexp_like (task_name, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.setString(3, regexp);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchMultiple(resultSet);
    }

    @Override
    public void persist(Task entity, Connection connection) throws SQLException {

    }

    @Override
    public void merge(Task entity, Connection connection) throws SQLException {

    }

    @Override
    public void remove(String userId, String entityId, Connection connection) throws SQLException {

    }

    @NotNull
    @Override
    public List<Task> getEntitiesList(@NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from task";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        return fetchMultiple(resultSet);
    }

    @Override
    public void loadEntities(@NotNull final List<Task> entities,
                             @NotNull final Connection connection) throws SQLException {
        @NotNull final String removeQuery = "delete from task";
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(removeQuery);
        @NotNull final String insertQuery = "insert into task (id, task_name, description, date_start, date_finish, user_id, project_id, status_id, system_time) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        for (Task task: entities) {
            @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, task.getId());
            preparedStatement.setString(2, task.getName());
            preparedStatement.setDate(3, Date.valueOf(task.getDateStart()));
            preparedStatement.setDate(4, Date.valueOf(task.getDateFinish()));
            preparedStatement.setString(5, task.getUserId());
            preparedStatement.setString(6, task.getProjectId());
            preparedStatement.setString(7, task.getStatus());
            preparedStatement.setLong(8, task.getSystemTime());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public List<Task> getSortedBySystemTime(String userId, Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?;";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Task> comparator = Comparator.comparingLong(Entity::getSystemTime);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> getSortedByDateStart(String userId, Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Task> comparator = Comparator.comparing(Entity::getDateStart);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> getSortedByDateFinish(String userId, Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Task> comparator = Comparator.comparing(Entity::getDateFinish);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Task> getSortedByStatus(String userId, Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Task> comparator = Comparator.comparing(Entity::getStatus);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    private Task fetchOne(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        resultSet.next();
        return fetchOne(resultSet);
    }

    private List<Task> fetchMultiple(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(_fetch(resultSet));
        }
        return tasks;
    }

    private Task _fetch(@NotNull final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final Task task = new Task();
        task.setId(resultSet.getString(FieldConst.ID));
        task.setName(resultSet.getString(FieldConst.TASK_NAME));
        task.setDescription(resultSet.getString(FieldConst.DESCRIPTION));
        task.setDateStart(resultSet.getDate(FieldConst.DATE_START).toString());
        task.setDateFinish(resultSet.getDate(FieldConst.DATE_FINISH).toString());
        task.setUserId(resultSet.getString(FieldConst.USER_ID));
        task.setStatus(resultSet.getString(FieldConst.STATUS_ID));
        task.setProjectId(resultSet.getString(FieldConst.PROJECT_ID));
        return task;
    }
}
