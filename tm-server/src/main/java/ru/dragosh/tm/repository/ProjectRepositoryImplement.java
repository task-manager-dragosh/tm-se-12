package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.entity.Entity;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.util.FieldConst;

import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class ProjectRepositoryImplement implements ProjectRepository {
    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId,
                                 @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?;";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        return fetchMultiple(resultSet);
    }

    @Nullable
    @Override
    public Project find(@NotNull final String projectName,
                        @NotNull final String userId,
                        @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where project_name = ? and user_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectName);
        statement.setString(2, userId);
        @Nullable final ResultSet resultSet = statement.executeQuery();
        return fetchOne(resultSet);
    }

    @Override
    public void removeAll(@NotNull final String userId,
                          @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "Delete from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findByStringPart(@NotNull final String userId,
                                          @NotNull final String str,
                                          @NotNull final Connection connection) throws SQLException {
        @NotNull final String regexp = "'*" + str + "*'";
        @NotNull final String query = "select * from project where user_id = ? and regexp_like (project_name, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, regexp);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return fetchMultiple(resultSet);
    }

    @Override
    public void persist(@NotNull final Project entity,
                        @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "insert into project (id, project_name, description, date_start, date_finish, user_id, status_id, system_time) values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.executeUpdate();
    }

    @Override
    public void merge(@NotNull final Project entity,
                      @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "update project set project_name = ? where id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, entity.getName());
        preparedStatement.setString(2, entity.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public void remove(@NotNull final String userId,
                       @NotNull final String entityId,
                       @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "delete from project where user_id = ? and id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, entityId);
        preparedStatement.executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> getEntitiesList(@NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        return fetchMultiple(resultSet);
    }

    @Override
    public void loadEntities(@NotNull final List<Project> entities,
                             @NotNull final Connection connection) throws SQLException {
        @NotNull final String removeQuery = "delete from project where project_name != ''";
        @NotNull final Statement statement = connection.createStatement();
        statement.executeUpdate(removeQuery);
        @NotNull final String insertQuery = "insert into project (id, project_name, description, date_start, date_finish, user_id, status_id, system_time) values (?,?,?,?,?,?,?,?)";

        for (Project project: entities) {
            @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);
            preparedStatement.setString(1, project.getId());
            preparedStatement.setString(2, project.getName());
            preparedStatement.setDate(3, Date.valueOf(project.getDateStart()));
            preparedStatement.setDate(4, Date.valueOf(project.getDateFinish()));
            preparedStatement.setString(5, project.getUserId());
            preparedStatement.setString(6, project.getStatus());
            preparedStatement.setLong(7, project.getSystemTime());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public List<Project> getSortedBySystemTime(@NotNull final String userId,
                                               @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?;";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Project> comparator = Comparator.comparingLong(Entity::getSystemTime);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Project> getSortedByDateStart(@NotNull final String userId,
                                              @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Project> comparator = Comparator.comparing(Entity::getDateStart);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Project> getSortedByDateFinish(@NotNull final String userId,
                                               @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Project> comparator = Comparator.comparing(Entity::getDateFinish);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<Project> getSortedByStatus(@NotNull final String userId,
                                           @NotNull final Connection connection) throws SQLException {
        @NotNull final String query = "select * from project where user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.getResultSet();
        Comparator<Project> comparator = Comparator.comparing(Project::getStatus);
        return fetchMultiple(resultSet).stream()
                .filter(entity -> entity.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    private Project fetchOne(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        resultSet.next();
        return _fetch(resultSet);
    }

    private List<Project> fetchMultiple(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(_fetch(resultSet));
        }
        return projects;
    }

    private Project _fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null)
            return null;
        @NotNull final Project project = new Project();
        project.setId(resultSet.getString(FieldConst.ID));
        project.setName(resultSet.getString(FieldConst.PROJECT_NAME));
        project.setDescription(resultSet.getString(FieldConst.DESCRIPTION));
        project.setDateStart(resultSet.getDate(FieldConst.DATE_START).toString());
        project.setDateFinish(resultSet.getDate(FieldConst.DATE_FINISH).toString());
        project.setSystemTime(resultSet.getLong(FieldConst.SYSTEM_TIME));
        project.setUserId(resultSet.getString(FieldConst.USER_ID));
        project.setStatus(resultSet.getString(FieldConst.STATUS_ID));
        return project;
    }
}

