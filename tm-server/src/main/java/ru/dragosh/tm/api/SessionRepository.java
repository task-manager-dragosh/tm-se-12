package ru.dragosh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface SessionRepository {
    List<Session> findAll(@NotNull Connection connection) throws SQLException;
    Session findOne(@NotNull String userId, @NotNull Connection connection) throws Exception;
    void persist(@NotNull Session session, @NotNull Connection connection) throws EntityIsAlreadyExistException, AccessForbiddenException, SQLException;
    void merge(@NotNull String sessionId, @NotNull Session session, @NotNull Connection connection) throws SQLException;
    void remove(@NotNull String sessionId, @NotNull Connection connection) throws Exception;
    void removeAll(@NotNull Connection connection) throws Exception;
    boolean contains(@NotNull final String id, @NotNull Connection connection) throws SQLException;
}
