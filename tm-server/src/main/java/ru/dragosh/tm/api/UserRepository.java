package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface UserRepository {
    User find(String login, String password, Connection connection) throws SQLException;
    User findByLogin(String login, Connection connection) throws SQLException;
    User findById(String id, Connection connection) throws SQLException;
    void persist(User user, Connection connection) throws SQLException;
    void merge(User user, Connection connection) throws SQLException;
    void remove(String userId, Connection connection) throws SQLException;

    List<User> getEntitiesList(Connection connection) throws SQLException;
    void loadEntities(List<User> entities, Connection connection) throws SQLException;
}
