package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Entity;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface Repository<E extends Entity> {
    void persist(E entity, Connection connection) throws ParseException, SQLException;
    void merge(E entity, Connection connection) throws SQLException;
    void remove(String userId, String entityId, Connection connection) throws SQLException;

    List<E> getEntitiesList(Connection connection) throws SQLException;
    void loadEntities(List<E> entities, Connection connection) throws SQLException;

    List<E> getSortedBySystemTime(String userId, Connection connection) throws SQLException;
    List<E> getSortedByDateStart(String userId, Connection connection) throws SQLException;
    List<E> getSortedByDateFinish(String userId, Connection connection) throws SQLException;
    List<E> getSortedByStatus(String userId, Connection connection) throws SQLException;
}
