package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface ProjectRepository extends Repository<Project> {
    List<Project> findAll(String userId, Connection connection) throws SQLException;
    Project find(String projectName, String userId, Connection connection) throws SQLException;
    void removeAll(String userId, Connection connection) throws SQLException;
    List<Project> findByStringPart(String userId, String str, Connection connection) throws SQLException;
}