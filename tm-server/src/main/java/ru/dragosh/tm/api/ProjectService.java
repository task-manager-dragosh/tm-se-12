package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Project;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface ProjectService {
    List<Project> findAll(String userId, Connection connection) throws SQLException;
    Project find(String projectName, String userId, Connection connection) throws SQLException;
    void persist(Project project, Connection connection) throws ParseException, SQLException;
    void merge(Project project, Connection connection) throws SQLException;
    void remove(String userId, String projectId, Connection connection) throws SQLException;
    void removeAll(String userId, Connection connection) throws SQLException;

    List<Project> getEntitiesList(Connection connection) throws SQLException;
    void loadEntities(List<Project> entities, Connection connection) throws SQLException;

    List<Project> findByStringPart(String userId, String str, Connection connection) throws SQLException;

    List<Project> getSortedBySystemTime(String userId, Connection connection) throws SQLException;
    List<Project> getSortedByDateStart(String userId, Connection connection) throws SQLException;
    List<Project> getSortedByDateFinish(String userId, Connection connection) throws SQLException;
    List<Project> getSortedByStatus(String userId, Connection connection) throws SQLException;
}