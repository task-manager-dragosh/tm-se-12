package ru.dragosh.tm.api;

import ru.dragosh.tm.entity.Task;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface TaskService {
    List<Task> findAll(String userId, String projectId, Connection connection) throws SQLException;
    Task find(String userId, String projectId, String nameTask, Connection connection) throws SQLException;
    void persist(Task task, Connection connection) throws ParseException, SQLException;
    void merge(Task task, Connection connection) throws SQLException;
    void remove(String userId, String taskId, Connection connection) throws SQLException;
    void removeAll(String userId, String projectId, Connection connection) throws SQLException;

    List<Task> getEntitiesList(Connection connection) throws SQLException;
    void loadEntities(List<Task> entities, Connection connection) throws SQLException;

    List<Task> findByStringPart(String userId, String projectId, String str, Connection connection) throws SQLException;

    List<Task> getSortedBySystemTime(String userId, Connection connection) throws SQLException;
    List<Task> getSortedByDateStart(String userId, Connection connection) throws SQLException;
    List<Task> getSortedByDateFinish(String userId, Connection connection) throws SQLException;
    List<Task> getSortedByStatus(String userId, Connection connection) throws SQLException;
}