package ru.dragosh.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.entity.Task;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TaskRepository extends Repository<Task> {
    List<Task> findAll(String userId, String projectId, Connection connection) throws SQLException;
    Task find(String userId, String projectId, String nameTask, Connection connection) throws SQLException;
    void removeAll(String userId, String projectId, Connection connection) throws SQLException;
    List<Task> findByStringPart(String userId, String projectId, String str, Connection connection) throws SQLException;
}