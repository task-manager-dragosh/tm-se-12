package ru.dragosh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;

import javax.management.relation.Role;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface SessionService {
    void validate(@Nullable Session session, Connection connection) throws AccessForbiddenException, SQLException;
    void validate(@Nullable Session session, @Nullable RoleType roleType, Connection connection) throws AccessForbiddenException, SQLException;
    void validateRole(@NotNull Connection connection, @Nullable Session session, @Nullable RoleType... roleTypes) throws AccessForbiddenException, SQLException;
    @Nullable Session sign(@Nullable Session session);
    @NotNull
    List<Session> getSessionList(Connection connection) throws SQLException;
    @NotNull Session openSession(@Nullable String login, @Nullable String password, @NotNull Connection connection) throws AccessForbiddenException, EntityIsAlreadyExistException, SQLException;
    void closeSession(@Nullable Session session, @NotNull Connection connection) throws Exception;
    String getServiceId();
}
