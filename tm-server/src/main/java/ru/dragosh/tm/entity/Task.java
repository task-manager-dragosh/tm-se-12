package ru.dragosh.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.enumeration.Status;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task implements Entity, Serializable {
    @NotNull
    @Getter
    @Setter
    private String id = UUID.randomUUID().toString();
    @Nullable
    @Getter
    @Setter
    private String name;
    @Nullable
    @Getter
    @Setter
    private String description;
    @Nullable
    @Getter
    @Setter
    private String dateStart;
    @Nullable
    @Getter
    @Setter
    private String dateFinish;
    @Nullable
    @Getter
    @Setter
    private String projectId;
    @Nullable
    @Getter
    @Setter
    private String userId;
    @NotNull
    @Getter
    @Setter
    private String status = Status.SCHEDULED.getStatusName();
    @Nullable
    @Getter
    @Setter
    private Long systemTime = System.currentTimeMillis();

    public Task(@Nullable final String name,
                @Nullable final String description,
                @Nullable final String dateStart,
                @Nullable final String dateFinish,
                @Nullable final String projectId,
                @Nullable final String userId) {
        this.name = name;
        this.description = description;
        this.setDateStart(dateStart);
        this.setDateFinish(dateFinish);
        this.projectId = projectId;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "UUID задачи: " + this.id + ";\n" +
                "Название задачи: " + this.name + ";\n" +
                "Описание задачи: " + this.description + ";\n" +
                "Дата начала выполнения задачи: " + this.dateStart + ";\n" +
                "Дата окончания выполнения задачи: " + this.dateFinish + ";" +
                "Статус: " + this.status + ";";
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;

        if (name == null)
            return false;

        return name.equals(task.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}