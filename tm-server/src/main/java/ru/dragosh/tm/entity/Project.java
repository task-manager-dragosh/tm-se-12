package ru.dragosh.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.enumeration.Status;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Project implements Entity, Serializable {
    @NotNull
    @Getter
    @Setter
    private String id = UUID.randomUUID().toString();
    @Nullable
    @Getter
    @Setter
    private String name;
    @Nullable
    @Getter
    @Setter
    private String description;
    @NotNull
    @Getter
    @Setter
    private String dateStart;
    @NotNull
    @Getter
    @Setter
    private String dateFinish;
    @NotNull
    @Getter
    @Setter
    private String userId;
    @NotNull
    @Getter
    @Setter
    private String status = Status.SCHEDULED.getStatusName();
    @NotNull
    @Getter
    @Setter
    private Long systemTime = System.currentTimeMillis();

    public Project(@Nullable final String name,
                   @Nullable final String description,
                   @Nullable final String dateStart,
                   @Nullable final String dateFinish,
                   @Nullable final String userId) {
        this.name = name;
        this.description = description;
        this.setDateStart(dateStart);
        this.setDateFinish(dateFinish);
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "UUID проекта: " + this.id + ";\n" +
                "Название проекта: " + this.name + ";\n" +
                "Описание проекта: " + this.description + ";\n" +
                "Дата начала проекта: " + this.dateStart + ";\n" +
                "Дата окончания проекта: " + this.dateFinish + ";\n" +
                "Статус: " + this.status + ";";
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;

        if (id == null || name == null)
            return false;

        return name.equals(project.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, dateStart, dateFinish);
    }
}