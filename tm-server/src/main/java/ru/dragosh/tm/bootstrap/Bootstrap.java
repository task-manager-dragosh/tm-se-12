package ru.dragosh.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.*;
import ru.dragosh.tm.endpoint.*;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.repository.ProjectRepositoryImplement;
import ru.dragosh.tm.repository.TaskRepositoryImplement;
import ru.dragosh.tm.repository.UserRepositoryImplement;
import ru.dragosh.tm.service.ProjectServiceImplement;
import ru.dragosh.tm.service.SessionServiceImplement;
import ru.dragosh.tm.service.TaskServiceImplement;
import ru.dragosh.tm.service.UserServiceImplement;
import ru.dragosh.tm.service.serializer.*;
import ru.dragosh.tm.util.ConnectionUtil;

import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.sql.SQLException;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    @Getter
    private final ProjectService projectService = new ProjectServiceImplement(new ProjectRepositoryImplement());

    @NotNull
    @Getter
    private final UserService userService = new UserServiceImplement(new UserRepositoryImplement());

    @NotNull
    @Getter
    private final TaskService taskService = new TaskServiceImplement(new TaskRepositoryImplement());

    @NotNull
    @Getter
    private final SessionService sessionService = new SessionServiceImplement(this);

    @NotNull
    @Getter
    private final DataBinServiceImplement dataBinServiceImplement = new DataBinServiceImplement();

    @NotNull
    @Getter
    private final FasterJsonServiceImplement fasterJsonServiceImplement = new FasterJsonServiceImplement();

    @NotNull
    @Getter
    private final FasterXmlServiceImplement fasterXmlServiceImplement = new FasterXmlServiceImplement();

    @NotNull
    @Getter
    private final JaxbJsonServiceImplement jaxbJsonServiceImplement = new JaxbJsonServiceImplement();

    @NotNull
    @Getter
    private final JaxbXmlServiceImplement jaxbXmlServiceImplement = new JaxbXmlServiceImplement();

    @NotNull
    @Getter
    private final Connection connection = ConnectionUtil.getConnection();

    public void init() {
        publishEndPoints();
        prepareUsers();
    }

    private void publishEndPoints() {
        Endpoint.publish("http://localhost:8080/ProjectEndPoint", new ProjectEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/UserEndPoint", new UserEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/FasterJsonEndPoint", new FasterJsonEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/FasterXmlEndPoint", new FasterXmlEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/JaxbJsonEndPoint", new JaxbJsonEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/JaxbXmlEndPoint", new JaxbXmlEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/DataBinEndPoint", new DataBinEndPointImplement(this));
        Endpoint.publish("http://localhost:8080/TaskEndPoint", new TaskEndPointImplement(this));

        System.out.println("http://localhost:8080/ProjectEndPoint?wsdl");
        System.out.println("http://localhost:8080/TaskEndPoint?wsdl");
        System.out.println("http://localhost:8080/UserEndPoint?wsdl");
        System.out.println("http://localhost:8080/FasterJsonEndPoint?wsdl");
        System.out.println("http://localhost:8080/FasterXmlEndPoint?wsdl");
        System.out.println("http://localhost:8080/JaxbJsonEndPoint?wsdl");
        System.out.println("http://localhost:8080/JaxbXmlEndPoint?wsdl");
    }

    private void prepareUsers() {
        User user1 = new User("user", "user", RoleType.USER);
        User user2 = new User("root", "root", RoleType.ADMIN);
        try {
            userService.persist(user1, connection);
            userService.persist(user2, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
