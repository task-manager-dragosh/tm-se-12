package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.UserRepository;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserServiceImplement implements UserService {
    @NotNull
    private final UserRepository userRepository;

    public UserServiceImplement(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public User find(@NotNull final String login,
                     @NotNull final String password,
                     @NotNull final Connection connection) throws SQLException {
        if (login == null || login.isEmpty())
            return null;
        if (password == null || password.isEmpty())
            return null;
        return userRepository.find(login, password, connection);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login,
                            @NotNull final Connection connection) throws SQLException {
        if (login == null || login.isEmpty())
            return null;
        return userRepository.findByLogin(login, connection);
    }

    @Override
    public User findById(@NotNull final String id,
                         @NotNull final Connection connection) throws SQLException {
        return userRepository.findById(id, connection);
    }

    @Override
    public void persist(@NotNull final User user,
                        @NotNull final Connection connection) throws SQLException {
        if (user == null)
            return;
        userRepository.persist(user, connection);
    }

    @Override
    public void merge(@NotNull final User user,
                      @NotNull final Connection connection) throws SQLException {
        if (user == null)
            return;
        userRepository.merge(user, connection);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return;
        userRepository.remove(userId, connection);
    }

    @NotNull
    @Override
    public List<User> getEntitiesList(@NotNull final Connection connection) throws SQLException {
        return userRepository.getEntitiesList(connection);
    }

    @Override
    public void loadEntities(@NotNull final List<User> entities, @NotNull final Connection connection) throws SQLException {
        if (entities == null)
            return;
        userRepository.loadEntities(entities, connection);
    }
}
