package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ProjectRepository;
import ru.dragosh.tm.api.ProjectService;
import ru.dragosh.tm.entity.Project;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public final class ProjectServiceImplement extends AbstractService<Project, ProjectRepository> implements ProjectService {
    @NotNull
    private final ProjectRepository projectRepository;

    public ProjectServiceImplement(@NotNull final ProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId,
                                 @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return projectRepository.findAll(userId, connection);
    }

    @Nullable
    @Override
    public Project find(@NotNull final String projectName,
                        @NotNull final String userId,
                        @NotNull final Connection connection) throws SQLException {
        if (projectName == null || projectName.isEmpty())
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        return projectRepository.find(projectName, userId, connection);
    }

    @Override
    public void removeAll(@NotNull final String userId,
                          @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return;
        projectRepository.removeAll(userId, connection);
    }

    @NotNull
    @Override
    public List<Project> findByStringPart(@NotNull final String userId,
                                          @NotNull final String str,
                                          @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        if (str == null || str.isEmpty())
            return Collections.emptyList();
        return projectRepository.findByStringPart(userId, str, connection);
    }

    @NotNull
    @Override
    public List<Project> getSortedBySystemTime(@NotNull final String userId,
                                               @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return projectRepository.getSortedBySystemTime(userId, connection);
    }

    @NotNull
    @Override
    public List<Project> getSortedByDateStart(@NotNull final String userId,
                                              @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return projectRepository.getSortedByDateStart(userId, connection);
    }

    @NotNull
    @Override
    public List<Project> getSortedByDateFinish(@NotNull final String userId,
                                               @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return projectRepository.getSortedByDateFinish(userId, connection);
    }

    @NotNull
    @Override
    public List<Project> getSortedByStatus(@NotNull final String userId,
                                           @NotNull final Connection connection) throws SQLException {
        if (userId == null || userId.isEmpty())
            return Collections.emptyList();
        return projectRepository.getSortedByStatus(userId, connection);
    }
}
