package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.dragosh.tm.api.Repository;
import ru.dragosh.tm.entity.Entity;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public abstract class AbstractService <E extends Entity, R extends Repository<E>>{
    @NotNull
    protected final R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    public void persist(@NotNull final E entity, @NotNull final Connection connection) throws ParseException, SQLException {
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        entity.setDateStart(dt.format(dt.parse(entity.getDateStart())));
        entity.setDateFinish(dt.format(dt.parse(entity.getDateFinish())));
        repository.persist(entity, connection);
    }

    public void merge(@NotNull final E entity,
                      @NotNull final Connection connection) throws SQLException {
        repository.merge(entity, connection);
    }

    public void remove(@NotNull final String userId,
                       @NotNull final String entityId,
                       @NotNull final Connection connection) throws SQLException {
        repository.remove(userId, entityId, connection);
    }

    @NotNull
    public List<E> getEntitiesList(@NotNull final Connection connection) throws SQLException {
        return repository.getEntitiesList(connection);
    }

    public void loadEntities(@NotNull final List<E> entities,
                             @NotNull final Connection connection) throws SQLException {
        repository.loadEntities(entities, connection);
    }
}
