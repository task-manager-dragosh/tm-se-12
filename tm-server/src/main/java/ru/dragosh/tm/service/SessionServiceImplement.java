package ru.dragosh.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.api.SessionRepository;
import ru.dragosh.tm.api.SessionService;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;
import ru.dragosh.tm.repository.SessionRepositoryImplement;
import ru.dragosh.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public final class SessionServiceImplement implements SessionService {
    @NotNull
    private final String serviceId = UUID.randomUUID().toString();

    @NotNull
    private final SessionRepository sessionRepository = new SessionRepositoryImplement();

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionServiceImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void validate(@Nullable final Session session,
                         @NotNull final Connection connection) throws AccessForbiddenException, SQLException {
        if(session == null)
            throw new AccessForbiddenException();
        if(session.getSignature() == null || session.getSignature().isEmpty())
            throw new AccessForbiddenException();
        if(session.getUserId() == null || session.getUserId().isEmpty())
            throw new AccessForbiddenException();
        if(session.getTimeStamp() == null)
            throw new AccessForbiddenException();
        @NotNull final Session temp = session.clone();
        if(temp == null)
            throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check)
            throw new AccessForbiddenException();
        if(!sessionRepository.contains(session.getId(), connection))
            throw new AccessForbiddenException();
    }

    @Override
    public void validate(@Nullable final Session session,
                         @Nullable final RoleType roleType,
                         @NotNull final Connection connection) throws AccessForbiddenException, SQLException {
        if(roleType == null)
            throw new AccessForbiddenException();
        validate(session, connection);
        if(session == null)
            throw new AccessForbiddenException();
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId, connection);
        if(user == null)
            throw new AccessForbiddenException();
        if(user.getRole() == null)
            throw new AccessForbiddenException();
        if(!roleType.equals(user.getRole()))
            throw new AccessForbiddenException();
    }

    @Override
    public void validateRole(@NotNull Connection connection, @Nullable Session session, @Nullable RoleType... roleTypes) throws AccessForbiddenException, SQLException {
        if (session == null)
            throw new AccessForbiddenException();
        if (roleTypes == null)
            throw new AccessForbiddenException();
        @Nullable User user = serviceLocator.getUserService().findById(session.getId(), connection);
        if (user == null)
            throw new AccessForbiddenException();
        Set<RoleType> roleTypeSet = new HashSet<>(Arrays.asList(roleTypes));
        if (!roleTypeSet.contains(user.getRole()))
            throw new AccessForbiddenException();
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = SignatureUtil.sign(session, "Task_Manager", 147);
        session.setSignature(signature);
        return session;
    }

    @NotNull
    @Override
    public List<Session> getSessionList(@NotNull final Connection connection) throws SQLException {
        return sessionRepository.findAll(connection);
    }

    @NotNull
    @Override
    public Session openSession(@Nullable final String login,
                               @Nullable final String password,
                               @NotNull final Connection connection) throws AccessForbiddenException, EntityIsAlreadyExistException, SQLException {
        if(serviceLocator == null)
            throw new AccessForbiddenException();
        @NotNull final UserService userService = serviceLocator.getUserService();
        @NotNull final User user = userService.find(login, password, connection);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimeStamp(1447L);
        @Nullable final Session signedSession = sign(session);
        if(signedSession == null)
            throw new AccessForbiddenException();
        sessionRepository.persist(signedSession, connection);
        return signedSession;
    }

    @Override
    public void closeSession(@Nullable final Session session,
                             @Nullable final Connection connection) throws Exception {
        validate(session, connection);
        sessionRepository.remove(session.getId(), connection);
    }

    @Override
    public String getServiceId() {
        return this.serviceId;
    }
}
