package ru.dragosh.tm.util;

import ru.dragosh.tm.enumeration.RoleType;

public class EntityUtil {
    public static RoleType getRoleById(String id) {
        return RoleType.ADMIN.getId().equals(id)? RoleType.ADMIN : RoleType.USER;
    }
}
