package ru.dragosh.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.entity.Domain;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.sql.Connection;

@NoArgsConstructor
@WebService
public final class FasterXmlEndPointImplement implements FasterXmlEndPoint {
    @NotNull
    private ServiceLocator serviceLocator;

    @NotNull
    private final Connection connection = serviceLocator.getConnection();

    public FasterXmlEndPointImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void save(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validate(session, RoleType.ADMIN, connection);
        @NotNull Domain domain = new Domain();
        domain.setUserList(serviceLocator.getUserService().getEntitiesList(connection));
        domain.setProjectList(serviceLocator.getProjectService().getEntitiesList(connection));
        domain.setTaskList(serviceLocator.getTaskService().getEntitiesList(connection));
        serviceLocator.getFasterXmlServiceImplement().save(domain);
    }

    @WebMethod
    public void load(@Nullable final Session session) throws Exception {
        if (session == null)
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validate(session, RoleType.ADMIN, connection);
        @Nullable final Domain domain = serviceLocator.getFasterXmlServiceImplement().Load();
        if (domain == null)
            throw new AccessForbiddenException();
        serviceLocator.getTaskService().loadEntities(domain.getTaskList(), connection);
        serviceLocator.getUserService().loadEntities(domain.getUserList(), connection);
        serviceLocator.getProjectService().loadEntities(domain.getProjectList(), connection);
        serviceLocator.getSessionService().closeSession(session, connection);
    }
}
