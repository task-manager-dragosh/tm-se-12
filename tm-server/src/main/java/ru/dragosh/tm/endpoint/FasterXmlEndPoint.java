package ru.dragosh.tm.endpoint;

import ru.dragosh.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebService
public interface FasterXmlEndPoint {
    @WebMethod
    void save(Session session) throws Exception;
    @WebMethod
    void load(Session session) throws Exception;
}
