package ru.dragosh.tm.endpoint;

import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.Task;
import ru.dragosh.tm.entity.containters.TaskList;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.text.ParseException;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TaskEndPoint {
    @WebMethod
    TaskList findAll(Session session, String projectId) throws Exception;
    @WebMethod
    Task find(Session session, String projectId, String taskName) throws Exception;
    @WebMethod
    void removeAll(Session session, String projectId) throws Exception;
    @WebMethod
    TaskList findByStringPart(Session session, String projectId, String part) throws Exception;
    @WebMethod
    void persist(Session session, Task Task) throws ParseException, Exception;
    @WebMethod
    void merge(Session session, Task task) throws Exception;
    @WebMethod
    void remove(Session session, String taskId) throws Exception;
    @WebMethod
    TaskList getSortedBySystemTime(Session session) throws Exception;
    @WebMethod
    TaskList getSortedByDateStart(Session session) throws Exception;
    @WebMethod
    TaskList getSortedByDateFinish(Session session) throws Exception;
    @WebMethod
    TaskList getSortedByStatus(Session session) throws Exception;
}
