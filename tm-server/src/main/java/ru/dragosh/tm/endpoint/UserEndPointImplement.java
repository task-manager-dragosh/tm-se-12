package ru.dragosh.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.api.UserService;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.User;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;
import ru.dragosh.tm.exception.EntityIsAlreadyExistException;
import ru.dragosh.tm.exception.EntityNotExistsException;
import ru.dragosh.tm.util.ConsoleUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.Connection;
import java.sql.SQLException;

@NoArgsConstructor
@WebService
public final class UserEndPointImplement implements UserEndPoint {
    @NotNull
    private ServiceLocator serviceLocator;

    @NotNull final Connection connection = serviceLocator.getConnection();

    public UserEndPointImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @WebMethod
    public Session authorisation(@WebParam(name = "session") @Nullable final Session session,
                                 @WebParam(name = "login") @Nullable final String login,
                                 @WebParam(name = "password") @Nullable final String password) throws AccessForbiddenException, EntityIsAlreadyExistException, EntityNotExistsException {
        if (!session.getSignature().isEmpty() && !session.getUserId().isEmpty() && session.getTimeStamp() != 0L) {
            throw new AccessForbiddenException();
        }
        if (login == null || login.isEmpty())
            throw new AccessForbiddenException();
        if (password == null || password.isEmpty())
            throw new AccessForbiddenException();

        UserService userService = serviceLocator.getUserService();
        User user = null;
        try {
            user = userService.find(login, ConsoleUtil.getHash(password), connection);
        if (user == null)
            throw new EntityNotExistsException();
            return serviceLocator.getSessionService().openSession(login, ConsoleUtil.getHash(password), connection);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new AccessForbiddenException();
        }
    }

    @WebMethod
    public Session registration(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "login") @Nullable final String login,
                                @WebParam(name = "password") @Nullable final String password) throws AccessForbiddenException, EntityIsAlreadyExistException {
        if (!session.getSignature().isEmpty() && !session.getUserId().isEmpty() && session.getTimeStamp() != 0L)
            throw new AccessForbiddenException();
        if (login == null || login.isEmpty())
            throw new AccessForbiddenException();
        if (password == null || password.isEmpty())
            throw new AccessForbiddenException();
        UserService userService = serviceLocator.getUserService();
        try {
            if (userService.findByLogin(login, connection) != null)
                throw new AccessForbiddenException();
            userService.persist(new User(login, ConsoleUtil.getHash(password), RoleType.USER), connection);
            return serviceLocator.getSessionService().openSession(login, ConsoleUtil.getHash(password), connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return session;
    }

    @WebMethod
    public Session updateLogin(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "newLogin") @Nullable final String newLogin
    ) throws AccessForbiddenException, EntityIsAlreadyExistException {
        if (!session.getSignature().isEmpty() && !session.getUserId().isEmpty() && session.getTimeStamp() != 0L)
            throw new AccessForbiddenException();
        if (session == null)
            throw new AccessForbiddenException();
        try {
            serviceLocator.getSessionService().validate(session, connection);
            if (newLogin == null || newLogin.isEmpty())
                throw new AccessForbiddenException();
            if (serviceLocator.getUserService().findByLogin(newLogin, connection) != null)
                throw new EntityIsAlreadyExistException();
            User user = serviceLocator.getUserService().findById(session.getId(), connection);
            user.setLogin(newLogin);
            serviceLocator.getUserService().merge(user, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return session;
    }

    @WebMethod
    public Session updatePassword(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "newPassword") @Nullable final String newPassword) throws AccessForbiddenException {
        if (!session.getSignature().isEmpty() && !session.getUserId().isEmpty() && session.getTimeStamp() != 0L)
            throw new AccessForbiddenException();
        if (session == null)
            throw new AccessForbiddenException();
        if (newPassword == null || newPassword.isEmpty())
            throw new AccessForbiddenException();
        User user = null;
        try {
            user = serviceLocator.getUserService().findById(session.getId(), connection);
            user.setPassword(ConsoleUtil.getHash(newPassword));
            serviceLocator.getUserService().merge(user, connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        throw new AccessForbiddenException();
    }

    @WebMethod
    public User getProfile(@WebParam(name = "session") @Nullable final Session session) throws AccessForbiddenException {
        if (!session.getSignature().isEmpty() && !session.getUserId().isEmpty() && session.getTimeStamp() != 0L)
            throw new AccessForbiddenException();
        if (session == null)
            throw new AccessForbiddenException();
        try {
            serviceLocator.getSessionService().validate(session, connection);
            return serviceLocator.getUserService().findById(session.getId(), connection);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new AccessForbiddenException();
        }
    }

    @WebMethod
    public Session logout(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (!session.getSignature().isEmpty() && !session.getUserId().isEmpty() && session.getTimeStamp() != 0L)
            throw new AccessForbiddenException();
        if (session == null)
            throw new AccessForbiddenException();
        try {
            serviceLocator.getSessionService().validate(session, connection);
            serviceLocator.getSessionService().closeSession(session, connection);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return new Session();
    }
}
