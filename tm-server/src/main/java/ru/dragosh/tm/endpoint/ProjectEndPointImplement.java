package ru.dragosh.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.dragosh.tm.api.ServiceLocator;
import ru.dragosh.tm.entity.Project;
import ru.dragosh.tm.entity.Session;
import ru.dragosh.tm.entity.containters.ProjectList;
import ru.dragosh.tm.enumeration.RoleType;
import ru.dragosh.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Collections;

@NoArgsConstructor
@WebService
public final class ProjectEndPointImplement implements ProjectEndPoint {
    @NotNull
    private ServiceLocator serviceLocator;

    @NotNull
    private final Connection connection = serviceLocator.getConnection();
    public ProjectEndPointImplement(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public ProjectList findAll(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        ProjectList projectList = new ProjectList();
        projectList.setProjectList(Collections.emptyList());
        if (session == null)
            return projectList;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        projectList.setProjectList(serviceLocator.getProjectService().findAll(session.getId(), connection));
        return projectList;
    }

    @WebMethod
    public Project find(@WebParam(name = "projectName") @Nullable final String projectName,
                        @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (projectName == null)
            return null;
        if (session == null)
            return null;
        if (projectName.isEmpty())
            throw new AccessForbiddenException();
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        return serviceLocator.getProjectService().find(projectName, session.getId(), connection);
    }

    @WebMethod
    public void persist(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project) throws Exception {
        if (project == null)
            return;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        SimpleDateFormat dt = new SimpleDateFormat("dd.mm.yyyy");
        project.setDateStart(dt.format(dt.parse(project.getDateStart())));
        project.setDateFinish(dt.format(dt.parse(project.getDateFinish())));
        serviceLocator.getProjectService().persist(project, connection);
    }

    @WebMethod
    public void merge(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project) throws Exception {
        if (project == null)
            return;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getProjectService().merge(project, connection);
    }

    @WebMethod
    public void remove(@WebParam(name = "session") @Nullable final Session session,
                       @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null)
            return;
        if (projectId == null)
            return;
        if (projectId.isEmpty())
            return;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getProjectService().remove(session.getId(), projectId, connection);
    }

    @WebMethod
    public void removeAll(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null)
            return;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        serviceLocator.getProjectService().removeAll(session.getId(), connection);
    }

    @WebMethod
    public ProjectList findByStringPart(@WebParam(name = "session") @Nullable final Session session,
                                        @WebParam(name = "str") @Nullable final String str
    ) throws Exception {
        ProjectList projectList = new ProjectList();
        projectList.setProjectList(Collections.emptyList());
        if (str == null)
            return projectList;
        if (str.isEmpty())
            return projectList;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        projectList.setProjectList(serviceLocator.getProjectService().findByStringPart(session.getId(), str, connection));
        return projectList;
    }

    @WebMethod
    public ProjectList getSortedBySystemTime(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        ProjectList projectList = new ProjectList();
        projectList.setProjectList(Collections.emptyList());
        if (session == null)
            return projectList;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        projectList.setProjectList(serviceLocator.getProjectService().getSortedBySystemTime(session.getId(), connection));
        return projectList;
    }

    @WebMethod
    public ProjectList getSortedByDateStart(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        ProjectList projectList = new ProjectList();
        projectList.setProjectList(Collections.emptyList());
        if (session == null)
            return projectList;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        projectList.setProjectList(serviceLocator.getProjectService().getSortedBySystemTime(session.getId(), connection));
        return projectList;
    }

    @WebMethod
    public ProjectList getSortedByDateFinish(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        ProjectList projectList = new ProjectList();
        projectList.setProjectList(Collections.emptyList());
        if (session == null)
            return projectList;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        projectList.setProjectList(serviceLocator.getProjectService().getSortedBySystemTime(session.getId(), connection));
        return projectList;
    }

    @WebMethod
    public ProjectList getSortedByStatus(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        ProjectList projectList = new ProjectList();
        projectList.setProjectList(Collections.emptyList());
        if (session == null)
            return projectList;
        serviceLocator.getSessionService().validate(session, connection);
        serviceLocator.getSessionService().validateRole(connection, session, RoleType.USER, RoleType.ADMIN);
        projectList.setProjectList(serviceLocator.getProjectService().getSortedBySystemTime(session.getId(), connection));
        return projectList;
    }
}